#### CpE 421: Embedded Systems Homework 5
#### Dakota Smith

##### 2. Create a local clone of the P-Boot repo (contains bootloader source code):
> [5 pts] What byte does the PinePhone's CPU look at for bootloader bytecode??
	-8 KiB from the start of the SD or eMMC block
##### 5. Explore the Official Linux kernel
>[5 pts] What is version the most recent kernel release?  
	-5.11.1
>[5 pts] What version is the latest "stable" kernel release?
	-5.11.1
##### 6. Explore Pine64's Linux kernel
> [5 pts] What is the name of the most active branch?
	- pine64-kernel-ubports-5.10.y-megi 
> [3 pts] What version of the Linux kernel do you think that branch is based on?
	- 5.10
> [3 pts] What is the most recent commit made to that branch?
	- defc72ec, added CI config, 2 months ago
> [3 pts] Who authored that commit?
	- Marius Gripsgard
> [3 pts] When did they author the commit?
	- 1 year ago
> [3 pts] Who authored the commit with the hash `9d640944`?  
	- Ondrej Jirman
> [5 pts] Does that person go by another name?
	- Megi
##### 7. Explore Megi's Linux kernel
> [3pts] Which branch of his repo do you think he uses for the PinePhone?
	-orange-pi-5.12	
> [3pts] What version of the kernel is that based on?
	-5.12
> [3pts] When was Megi's latest commit to the pp-5.11 branch?
	-9 days ago
> [3pts] What file was modified in that commit?
	- arm64: Dts; sun50i-pinepjone: Add interrupt pin for WiFi
##### 8. Download a Pre-Built copy of Megi's Linux kernel
> [3 pts] Fetch Megi's key from the terminal using `curl <paste-link-here>` (screenshot)
Notice that `curl` reads content returned by accessing an internet url and prints the content
to `stdout` (which by default is your terminal)
	-
>[3 pts] Try `curl www.google.com` (screenshot) and see what comes back.  What kind of file is that?
	-
>[3 pts] This time, fetch megi's key but redirect stdout to a file (save in repo) called 'megi.key':  `curl <url-here>   >  megi.key`
	-
#### 9.  Setup a build environment to make LuneOS:
> [3pts] Print value of variable.

#### 10. Build Lune OS
. [10 pts] take screenshot prompts.png
. [10 pts] take screenshot bitbake.png
